// underflowInt.c
#include <stdio.h>
#include <limits.h>
#include <assert.h>

int main(void)
{
  assert(sizeof(int)==4);

  unsigned  ui = 0;
  signed    si = INT_MIN;  // -2147483648

  ui -= 1;
  si -= 1;

  printf("%u %d\n", ui, si);
  return 0;
}
