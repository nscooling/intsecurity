#include <stdio.h>

int main(void)
{
  unsigned int ui = 16;

  printf("%u * 16 = %u\n", ui, ui*16);
  printf("%u << 4 = %u\n", ui, ui << 4);
  return 0;
}
