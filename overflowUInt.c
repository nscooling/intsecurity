// overflowInt.c
#include <stdio.h>
#include <limits.h>

int main(void)
{
	unsigned  a = UINT_MAX;  // 4294967295
	unsigned  b =        1;
	unsigned  c =        0;

    c = a + b;
	printf("Result is %u + %u = %u\n", a, b, c);
	return 0;
}
