// bufferOverflow.c
// $ ./bufferOverflow -2147482047
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>

#define MAX_BUF_SZ 1024

int copySize;
int s[MAX_BUF_SZ*2];	// source buffer
int d[MAX_BUF_SZ];	// destination buffer
int c[MAX_BUF_SZ];	// corrupted buffer

void populateBuffer_s(void); // randomly fill buffer

int main(int argc, char** argv)
{
  assert(sizeof(int)==4);
  populateBuffer_s();
  printf("s[1024] %d c[0] %d\n", s[1024], c[0]);

  copySize = atoi(argv[1]);

  if(copySize > MAX_BUF_SZ) {
    puts("copySize too large");
    return -1;
  }
  printf("About to copy %u bytes\n", copySize*sizeof(int));
  memcpy(d,s,copySize*sizeof(int));

  printf("s[1024] %d c[0] %d\n", s[1024], c[0]);
  return 0;
}

void populateBuffer_s(void)
{
  for(int i = 0; i < MAX_BUF_SZ*2; ++i) {
    s[i] = rand()*100;
  }
}
