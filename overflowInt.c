// overflowInt.c
#include <stdio.h>
#include <limits.h>

int main(void)
{
	signed  a = INT_MAX;  // 
	signed  b =        1;
	signed  c =        0;

    	c = a + b;
	printf("Result is %d + %d = %d\n", a, b, c);
	return 0;
}
