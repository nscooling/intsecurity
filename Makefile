SRCS = $(wildcard *.c)

PROGS = $(patsubst %.c,%,$(SRCS))

CC=gcc
CFLAGS=-std=c99 -Wall 

all: $(PROGS)

%: %.c
	$(CC) $(CFLAGS)  -o $@ $<

clean:
	rm -rf $(PROGS)

